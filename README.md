# Docker with Spring-Data Redis

## Dockerized SpringData Redis Application
Clone of https://github.com/spring-guides/gs-messaging-redis.git

### Prequesites:
Docker Desktop installed.

## Multi-stage Dockefile
    FROM openjdk:8-jdk-alpine as build
    WORKDIR /workspace/app

    COPY mvnw .
    COPY .mvn .mvn
    COPY pom.xml .
    COPY src src

    RUN ./mvnw install -DskipTests
    RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)

    FROM openjdk:8-jre-alpine
    VOLUME /tmp
    ARG DEPENDENCY=/workspace/app/target/dependency
    COPY --from=build ${DEPENDENCY}/BOOT-INF/lib /app/lib
    COPY --from=build ${DEPENDENCY}/META-INF /app/META-INF
    COPY --from=build ${DEPENDENCY}/BOOT-INF/classes /app
    ENTRYPOINT ["java","-cp","app:app/lib/*","com.example.messagingredis.MessagingRedisApplication"]


### application.properties
  spring.redis.database=0 
  spring.redis.host=redis
  spring.redis.timeout=60000

### Login to Docker Hub.
Build Docker image of Redis client. Please update the Docker Hub User ID:
  docker build -t <UserID>/redis-client.

### Push the image to Docker Hub.
  docker push <UserID>/redis-client

## Create Bridge Network
  docker network create --driver bridge my-network

## Launch Redis server in Docker container, and expose on port 6379 in 'my-network'.
Note container name should match spring.redis.host in application.properties.

  docker run -d -p 6379:6379 --name=redis --network my-network redis

### Launch Redis client in Docker container on same network.
  docker run --rm --name messages --network my-network ptking777/messages
